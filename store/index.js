// Here I defined the source of truth for `User` objects,
// that file contains information about users required
// to perform login / logout.

import Mock from '~/assets/data/users'
import { setUser, deleteUser } from '~/assets/scripts/storage'

// Here I'm defining a state representing the `User` structure.

export const state = () => ({
  user: null,
})

// I'm using `isAuthorized` getter to check current authorization status
// It simply checks that fields of `User` structure are not empty.

export const getters = {
  isAuthorized(state) {
    return !!state.user
  }
}

// Here I`m defining mutations types, so i can reference them
// by the constants in the following code.

const SET_USER = 'SET_USER'

// The following is an implementation of the mutations types.
// Here I defined only one to set the whole `User` structure
// in one run.

export const mutations = {
  [SET_USER](state, user) {
    state.user = user
  }
}

// Vefiries that provided `user` object is matched with a
// user data from `Mock`.

async function verifyUser(user) {
  const { username, password } = user

  for (const item of Mock) {
    const matched = (
      item.username === username &&
      item.password === password
    )

    if (matched) {
      return item
    }
  }

  return false
}

// Here i`m defining actions types, so i can reference them
// the same way i done with the mutations types.

export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'

// And the following are my API to handle the login / logout flow.
// I`m exporting it and it will be used outside of the store.

export const actions = {
  async [LOGIN]({ commit }, payload) {
    const match = await verifyUser(payload)
    if (!match) {
      throw new Error('User data is invalid')
    }
    commit(SET_USER, match)
    await setUser(match)
  },
  async [LOGOUT]({ commit, getters }) {
    if (getters.isAuthorized) {
      commit(SET_USER, null)
      await deleteUser()
    }
  }
}
