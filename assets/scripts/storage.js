// Here i`m defining an API for using `Storage` object.
// I will store the user data within the choosen type
// of storage, so user can navigate pages and keeping
// session.

const STORAGE = window.localStorage

// Wrapper function that fetches and deserializes user object
// from defined type of `Storage`.

export async function getUser(user) {
  const data = STORAGE.getItem('user')
  if (!!data) {
    return JSON.parse(data)
  }
}

// Another wrapper function that serializes and sets user object
// to the defined type of `Storage`.

export async function setUser(user) {
  STORAGE.setItem('user', JSON.stringify(user))
}


// Wrapper function that removes user from `Storage`.

export async function deleteUser() {
  STORAGE.removeItem('user')
}
