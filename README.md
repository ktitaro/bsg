# bsg-group

## Запуск

Чтобы запустить приложение в докере, используйте docker-compose:

```sh
$ docker-compose up
```

---

Запуск без докера:

```sh
$ npm install // Устанавливаем зависимости
$ npm run generate // Генерирует статический сайт
$ npm run start // Запускаем сервер статики
```

## Тестирование

Что бы проверить авторизацию воспользуйтесь данными ниже

### Alice

username: alice  
password: secret

---

### Bob

username: bob  
password: qwerty
