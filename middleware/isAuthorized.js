// This middleware allowed only authorized users.
// First it checks is there a user in the store,
// then it checks any user in the `Storage`.

import { LOGIN } from '~/store'
import { getUser } from '~/assets/scripts/storage'

export default async ({ store, redirect, route }) => {
  // If user is authroized, simple pass the request
  const isAuthorized = store.getters['isAuthorized']
  if (isAuthorized) {
    return
  }

  // If there is no authorized user, then check if there
  // is anything in `Store`.
  const existedUser = await getUser()
  if (!existedUser) {
    if (route.fullPath === '/') {
      return
    }
    redirect('/')
  }

  // If `Store` has something, then try to login using
  // that data.
  try {
    await store.dispatch(LOGIN, existedUser)
  } catch (err) {
    if (route.fullPath === '/') {
      return
    }
    redirect('/')
  }
}
